/** 
 * Class containing bit arithmatic and logic operations
 */
class BitOperations {

    /**
     * Constructor
     * @param {Stirng} undef representation of undefined
     */
    constructor(undef = this.undef) {
        this.undef = "undef";
    };

    getUndef() {
        return this.undef;
    }

    /**
     * Adds 2 bit strings. Provides sum and carry out results
     * @param {String} bitString1 first bit string
     * @param {String} bitString2 second bit string
     */
    addBitStrings(bitString1, bitString2) {
        let results = {
            "sum": '',
            "carryOut": ''
        };
        //console.log(bitString1 + "   " + bitString2)
        if (this.isBitString(bitString1) && this.isBitString(bitString2)) {
            const longestLen = Math.max(bitString1.length, bitString2.length);
            const bitsOne =
                this.signExtend(bitString1, longestLen, bitString1.charAt(bitString1.length - 1));
            const bitsTwo =
                this.signExtend(bitString2, longestLen, bitString2.charAt(bitString2.length - 1));
            var lastCarryOut = 0;

            for (var x = longestLen - 1; x >= 0; x--) {
                var b1 = parseInt(bitsOne.charAt(x));
                var b2 = parseInt(bitsTwo.charAt(x));
                var reply = this.addBits(b1, b2, lastCarryOut);
                results.sum = reply.sum + results.sum;
                lastCarryOut = parseInt(reply.carryOut);

            }
            results.carryOut = lastCarryOut;

        } else {
            results.sum = this.undef;
            results.carryOut = this.undef;
        }
        return results;
    }

    /**
     * Adds 2 bits and a carry in
     * @param {Number} bit1 first bit to add
     * @param {Number} bit2 second bit to add
     * @param {Number} carryIn carry in bit (default to 0 if not supplied)
     * returns {sum, carryOut}. Contains sum/carryOut or {undef,undef} if params are not bits
     */
    addBits(bit1, bit2, carryIn = 0) {
        let results = {
            "sum": this.undef,
            "carryOut": this.undef
        };

        switch (bit1 + bit2 + carryIn) {
            case 0:
                results.sum = 0;
                results.carryOut = 0;
                break;
            case 1:
                results.sum = 1;
                results.carryOut = 0;
                break;
            case 2:
                results.sum = 0;
                results.carryOut = 1;
                break;
            case 3:
                results.sum = 1;
                results.carryOut = 1;
                break;
            default:
                results.sum = this.undef;
                results.carryOut = this.undef;
        }
        return results;
    }

    /**
     * Sign extends bitString with 'pad' to 'length'
     * @param {String} bitString
     * @param {Number} length
     * @param {char} pad
     * returns sign extended bitString or 'undef' is bitString is not a bit string
     */
    signExtend(bitString, length, pad) {
        var result = '';
        var padlen = length - bitString.length;

        if (this.isBitString(bitString)) {
            for (var x = 0; x < padlen; x++) {
                result += pad;
            }
        } else {
            return 'undef';
        }
        return result + bitString;
    }

    /**
     * Determines if bitString is a bit string
     * @param {String} bitString
     * returns true if bitString is a bit string. Returns false otherwise
     */
    isBitString(bitString) {
        return /^[01]+$/.test(bitString);
    }

    /**
     * Inverts supplied bit string.
     * @param {String} bitString bit string to invert
     * returns invert of bitString or 'undef' if bitString is not a bit string
     */
    not(bitString) {
        let result = '';

        if (this.isBitString(bitString)) {
            for (var x = 0; x < bitString.length; x++) {
                if (bitString.charAt(x) === '0') {
                    result += '1';
                }
                if (bitString.charAt(x) === '1') {
                    result += '0';
                }
            }
        } else {
            result = 'undef';
        }
        return result;
    }

    /**
     * Performs bit-wise AND on 2 bit strings. Returns result or 'undef' if any inputs are not bit strings
     * @param {String} bitString1 first bit string to and
     * @param {String} bitString2 second bit string to and
     */
    and(bitString1, bitString2) {
        var result = "";
        if (this.isBitString(bitString1) && this.isBitString(bitString2)) {
            const longestLen = Math.max(bitString1.length, bitString2.length);
            const bitsOne = this.signExtend(bitString1, longestLen, '0');
            const bitsTwo = this.signExtend(bitString2, longestLen, '0');

            for (var x = 0; x < longestLen; x++) {
                if ((bitsOne.charAt(x) === '1') && (bitsTwo.charAt(x) === '1')) {
                    result += "1";
                } else {
                    result += "0";
                }
            }
        } else {
            result = this.undef;
        }

        return result
    }

    /**
     * Performs bit-wise OR on 2 bit strings. Returns result or 'undef' if any inputs are not bit strings
     * @param {String} bitString1 first bit string to and
     * @param {String} bitString2 second bit string to and
     */
    or(bitString1, bitString2) {
        var result = "";
        if (this.isBitString(bitString1) && this.isBitString(bitString2)) {
            const longestLen = Math.max(bitString1.length, bitString2.length);
            const bitsOne = this.signExtend(bitString1, longestLen, '0');
            const bitsTwo = this.signExtend(bitString2, longestLen, '0');

            for (var x = 0; x < longestLen; x++) {
                if ((bitsOne.charAt(x) === '1') || (bitsTwo.charAt(x) === '1')) {
                    result += "1";
                } else {
                    result += "0";
                }
            }
        } else {
            result = this.undef;
        }
        return result
    }

    /**
     * Converts bit string to decimal
     * @param {String} bitString bit string to convert
     */
    convert(bitString) {
        let results = {
            "decimal": this.undef,
            "octal": this.undef,
            "hexadecimal": this.undef,
            "ascii": this.undef
        };

        if ((typeof bitString === 'string') && (this.isBitString(bitString))) {
            results.decimal = parseInt(bitString, 2).toString(10);
            results.octal = parseInt(bitString, 2).toString(8);
            results.hexadecimal = parseInt(bitString, 2).toString(16);
            var dec = parseInt(results.decimal);
            if ((dec > 31) && (dec < 126)) {
                results.ascii = String.fromCharCode(dec)
            } else {
                results.ascii = "non-printable"
            }
        }
        return results;
    }

    /**
     * Flips bit string without evaluting 2's comp pos/neg. Returns negation or undef id invalid input
     * @param {String} bitString 
     * @returns {String} bit string with all bits inverted
     */
    flipBits(bitString) {
        let result = '';
        if ((typeof bitString === 'string') && (this.isBitString(bitString))) {
            for (const bit of bitString) {
                result += (bit === '1' ? '0' : "1");
            }
        } else {
            result = this.undef;
        }
        return result;
    }

    /**
     * Adds '1' to the bit string without a 2's complement conversion
     * @param {String} bitString 
     * @returns {String} bit string + 1 without 2's comp conversion
     */
    addOne(bitString) {
        let result = '';
        let bitOne = this.signExtend('1', bitString.length, 0);
        let lastCarryOut = 0;

        if ((typeof bitString === 'string') && (this.isBitString(bitString))) {
            for (var x = bitString.length - 1; x >= 0; x--) {
                var b1 = parseInt(bitString.charAt(x));
                var b2 = parseInt(bitOne.charAt(x));
                var reply = this.addBits(b1, b2, lastCarryOut);
                result = reply.sum + result;
                lastCarryOut = parseInt(reply.carryOut);
            }
        } else {
            result = this.undef;
        }
        return result;
    }

    /**
     * Converts bit string to correctly signed value
     * Modified from https://stackoverflow.com/questions/51204773/parsing-of-binary-twos-complement-number-to-decimal
     * @param {String} bitString 
     * @returns {String} signed int conversion of bit string
     */
    getSignedInteger(bitString) {
        let result = '';

        if ((typeof bitString === 'string') && (this.isBitString(bitString))) {
            if (bitString[0] === '1') {
                let inverse = '';
                for (let i = 0; i < bitString.length; i++) {
                    inverse += (bitString[i] === '0' ? '1' : '0');
                }
                result = (parseInt(inverse, 2) + 1) * -1;
            } else {
                result = parseInt(bitString, 2);
            }
        }
        else {
            result = this.undef
        }
        return result;
    }

    /**
     * converts bit string to 2's complement (signed int) value
     * @param {String} bitString 
     * @returns {object} "onesComp, binary, decimal"
     */
    twosComplement(bitString) {
        let results = {
            "onesComp": this.undef,
            "binary": this.undef,
            "decimal": this.undef
        }

        if ((typeof bitString === 'string') && (this.isBitString(bitString))) {
            results.onesComp = this.flipBits(bitString);
            results.binary = this.addOne(results.onesComp);
            results.decimal = this.getSignedInteger(results.binary).toString();
        }
        return results;
    }
}

// export default BitOperations;
if (typeof module !== 'undefined' && typeof module.exports !== 'undefined') { module.exports = BitOperations }
// module.exports = BitOperations
// })(typeof exports === 'undefined' ? this['BitOperations'] = {} : exports);

// if (typeof module !== 'undefined' && typeof module.exports !== 'undefined')
// module.exports = BitOperations;
// else
// window.BitOperations = BitOperations;
// })();