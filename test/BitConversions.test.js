/**
 * Jest test suite for Bit Operations
 */
var BitOps = require('../src/BitOperations.js');
var bitOps;
var undef;

/**
 * Setup before starting tests
 */
beforeAll(() => {
    bitOps = new BitOps(-1);
    undef = bitOps.getUndef();
})

/**
 * Tests for 2's Complement
 */
describe('Flip bits', () => {

    test('Test flip(01)', () => {
        var result = bitOps.flipBits('01');
        expect(result).toBe('10');
    });

    test('Test flip(11111)', () => {
        var result = bitOps.flipBits('11111');
        expect(result).toBe('00000');
    });

    test('Test flip(101010)', () => {
        var result = bitOps.flipBits('101010');
        expect(result).toBe('010101');
    });

    test('Test flip(0101010)', () => {
        var result = bitOps.flipBits('0101010');
        expect(result).toBe('1010101');
    });

    test('Test flip(00000)', () => {
        var result = bitOps.flipBits('00000');
        expect(result).toBe('11111');
    });

    test('Test flip(bad data)', () => {
        var result = bitOps.flipBits('junk');
        expect(result).toBe(undef);
    });
});


/**
 * Tests for getSignedInteger
 */
describe('getSignedInteger', () => {

    test('Test getSignedInteger(01)', () => {
        var result = bitOps.getSignedInteger('01');
        expect(result).toBe(1);
    });

    test('Test getSignedInteger(11)', () => {
        var result = bitOps.getSignedInteger('11');
        expect(result).toBe(-1);
    });

    test('Test getSignedInteger(101)', () => {
        var result = bitOps.getSignedInteger('101');
        expect(result).toBe(-3);
    });

    test('Test getSignedInteger(011)', () => {
        var result = bitOps.getSignedInteger('011');
        expect(result).toBe(3);
    });
});


/**
 * Tests for 2's Complement
 */
describe('Twos Complement', () => {

    test('Test twosComplement(01)', () => {
        var result = bitOps.twosComplement('01');
        expect(result).toHaveProperty("binary", '11');
        expect(result).toHaveProperty("decimal", '-1');
        expect(result).toHaveProperty("onesComp", '10');
    });

    test('Test twosComplement(0111)', () => {
        var result = bitOps.twosComplement('0111');
        expect(result).toHaveProperty("binary", '1001');
        expect(result).toHaveProperty("decimal", '-7');
        expect(result).toHaveProperty("onesComp", '1000');
    });
});
